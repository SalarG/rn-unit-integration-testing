import authReducer from '../src/reducers/auth';
import { CHANGE_AUTH } from '../src/actions/index';

it('handles actions of type CHANGE_AUTH', () => {
    const action = {
        type: CHANGE_AUTH,
        payload: true
    }
    const newState = authReducer(false, action)

    expect(newState).toEqual(true)
})

it('handles action with unknow type', () => {
    const newState = authReducer(false, { type: 'ajfas' })
    expect(newState).toEqual(false)

})