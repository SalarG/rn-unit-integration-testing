import React, { Component } from 'react';
import { View, Text, Button, CheckBox, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { changeAuth } from './actions/index';

class AuthScreen extends Component {
    state = {
        email: '',
        pass: '',
    }

    _handleChange = async (e, name) => {
        //  console.log(e)
        const obj = {}
        obj[name] = e
        this.setState(obj, () => {
            console.log('setState', this.state)
        })
    }

    handleSubmit = () => {
        this.props.changeAuth(!this.props.auth)
        this.setState({ email: '', pass: '' }, () => {
            console.log('handleSubmit, this.state', this.state);
        })
        this.props.navigation.navigate('Comment')
    }

    buttonTitle = () => {
        const { auth } = this.props
        if (!auth) {
            return 'Login'
        }
        return 'Log out'
    }

    render() {
        return (
            <View>
                <Text testID="emailTitle">Email</Text>
                <TextInput
                    testID="enterEmail"
                    placeholder='enterEmail'
                    onChangeText={(e) => this._handleChange(e, 'email')}
                    value={this.state.email}
                />
                <Text testID="passTitle">Password</Text>
                <TextInput
                    testID="enterPass"
                    placeholder='enter password'
                    onChangeText={(e) => this._handleChange(e, 'pass')}
                    value={this.state.pass}
                />
                <Button
                    testID="authButton"
                    title={this.buttonTitle()}
                    onPress={this.handleSubmit}
                />
            </View>
        )
    }
}

const mapStateToProps = state => {

    return {
        auth: state.auth
    }
}
export default connect(mapStateToProps, { changeAuth })(AuthScreen);
//export default AuthScreen;
