import { FETCH_COMMENTS } from '../actions/index'


export default (state = null, action) => {
    switch (action.type) {
        case FETCH_COMMENTS:
            return action.payload
        default:
            return state
    }
}