export const CHANGE_AUTH = 'change_auth';
export const FETCH_COMMENTS = 'fetch_cpmments'

import axios from 'axios';

export const changeAuth = (auth) => {
    return {
        type: CHANGE_AUTH,
        payload: auth
    }
}

export const loadComments = async () => {
    const response = await axios.get('http://jsonplaceholder.typicode.com/comments')

    return {
        type: FETCH_COMMENTS,
        payload: response.data
    }
}