import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import AuthScreen from '../AuthScreen'
import CommentScreen from '../screens/CommentScreen';

const mainNavigator = createSwitchNavigator({
    Auth: AuthScreen,
    Comment: CommentScreen
})
const AppContainer = createAppContainer(mainNavigator);

export default AppContainer;