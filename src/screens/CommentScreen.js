import React, { Component } from 'react';
import { SafeAreaView, Button, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { loadComments } from '../actions/index'

class CommentScreen extends Component {
    handlePress = () => {
        this.props.loadComments()
    }
    renderComments = () => {
        console.log('comments:', this.props.comments)

        if (this.props.comments) {
            return (
                <FlatList
                    data={this.props.comments}
                    renderItem={({ item }) => <Text>{item.email}</Text>}
                    keyExtractor={item => item.id}
                    testId='flatList'
                />
            )
        }
    }
    render() {
        return (
            <SafeAreaView>
                <Button
                    testID="commentsButton"
                    title='Show Comments'
                    onPress={this.handlePress}
                />
                {this.renderComments()}
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        comments: state.comments
    }
}
export default connect(mapStateToProps, { loadComments })(CommentScreen);