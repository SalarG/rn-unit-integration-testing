import React, { ReactElement, Children } from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import App from '../App';
import AppContainer from '../src/navigation/index'
import Adapter from 'enzyme-adapter-react-16';
import AuthScreen from '../src/AuthScreen';
import CommentScreen from '../src/screens/CommentScreen';
import Root from '../src/Root';


Enzyme.configure({ adapter: new Adapter() });

// const mainNavigator = createSwitchNavigator({
//     Auth: AuthScreen,
//     Comment: CommentScreen
// })
// const TestNavigator = () => {
//     const AppContainer = createAppContainer(mainNavigator);
//     return <AppContainer />
// }

const createMockProps = (props) => ({
    createAppContainer: jest.fn().mockReturnValue(function NavigationContainer(props) { return null; }),
    navigation: { navigate: jest.fn() },
    loginSuccess: jest.fn(),
    isSubmitting: true,
    ...props
})

jest.mock('react-navigation', () => ({ withNavigation: component => component}));

const mockProps = {
    createAppContainer: jest.fn().mockReturnValue(function NavigationContainer(props) { return null; }),
    navigation: { navigate: jest.fn() },
    createSwitchNavigator: jest.fn(),
    loginSuccess: jest.fn(),
    isSubmitting: true
}
describe('App Component', () => {
    it('should render without issues', () => {
        let props = createMockProps({});
        const component = mount(
            <Root>
                <AppContainer {...mockProps}/>
            </Root>
        );
        expect(component.length).toBe(1);
    })

    // it('should contain LandingPage', () => {
    //     let props = createMockProps({});
    //     const component = shallow(
    //         <App {...props} />
    //     )
    //     expect(component.find(AuthScreen).length).toBe(1)
    // })
})
