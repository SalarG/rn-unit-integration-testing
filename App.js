import React from 'react';
import {
  StyleSheet, SafeAreaView
} from 'react-native';
import { Provider } from 'react-redux'
import AuthScreen from './src/AuthScreen';
import Root from './src/Root'
import AppContainer from './src/navigation/index';

// **ToDO react navigation is not linked to Android

const App = () => {
  return (
    <Root>
      <SafeAreaView style={styles.container}>
        <AppContainer />
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default App;
