// React, React-Dom, React-Test-Renderes Versions must be the same

const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

Enzyme.configure({ adapter: new Adapter() });

// this is the set up if you wanna use mock renderer ///////
// require('react-native-mock-render/mock');
// const { JSDOM } = require('jsdom');

// const jsdom = new JSDOM();
// const { window } = jsdom;

// function copyProps(src, target) {
//   const props = Object.getOwnPropertyNames(src)
//     .filter(prop => typeof target[prop] === 'undefined')
//     .map(prop => Object.getOwnPropertyDescriptor(src, prop));
//   Object.defineProperties(target, props);
// }

// global.window = window;
// global.document = window.document;
// global.navigator = {
//   userAgent: 'node.js',
// };
// copyProps(window, global);

// // Setup adapter to work with enzyme 3.2.0


// // Ignore React Web errors when using React Native
// // allow other errors to propagate if they're relevant
// const suppressedErrors = /(React does not recognize the.*prop on a DOM element|Unknown event handler property|is using uppercase HTML|Received `true` for a non-boolean attribute `accessible`|The tag.*is unrecognized in this browser)/
// const realConsoleError = console.error
// console.error = message => {
//   if (message.match(suppressedErrors)) {
//     return
//   }
//   realConsoleError(message)
// }



// in setupTests.js which is configured as jest's setup file.
// Adapted from https://blog.joinroot.com/mounting-react-native-components-with-enzyme-and-jsdom/ and
// https://stackoverflow.com/questions/42039383/how-to-use-enzyme-for-react-native-with-jest

// import { JSDOM } from 'jsdom'
// // used to fully render native components as react components
// import 'react-native-mock-render/mock'

// const Enzyme = require('enzyme');
// const Adapter = require('enzyme-adapter-react-16');

// Enzyme.configure({ adapter: new Adapter() });
// // copy props needed to render HTML from JSDOM window to node's global object
// function copyProps(src, target) {
//   Object.getOwnPropertyNames(src)
//     .filter(prop => typeof target[prop] === 'undefined')
//     .forEach(prop => {
//       // eslint-disable-next-line no-param-reassign
//       target[prop] = src[prop]
//     })
// }

// const { window } = new JSDOM()

// global.window = window
// global.document = window.document
// global.navigator = {
//   userAgent: 'node.js',
// }
// copyProps(window, global)

// // react doesn't like some of the props that are set on native components (that eventually are set on DOM nodes, so suppress those warnings
// const suppressedErrors = /(React does not recognize the.*prop on a DOM element|Unknown event handler property|is using uppercase HTML|Received `true` for a non-boolean attribute `accessible`|The tag.*is unrecognized in this browser)/
// // eslint-disable-next-line no-console
// const realConsoleError = console.error
// // eslint-disable-next-line no-console
// console.error = message => {
//   if (message.match(suppressedErrors)) {
//     return
//   }
//   realConsoleError(message)
// }

//https://airbnb.io/enzyme/docs/guides/jsdom.html
const { JSDOM } = require('jsdom');

const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
const { window } = jsdom;

function copyProps(src, target) {
  Object.defineProperties(target, {
    ...Object.getOwnPropertyDescriptors(src),
    ...Object.getOwnPropertyDescriptors(target),
  });
}

global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'node.js',
};
global.requestAnimationFrame = function (callback) {
  return setTimeout(callback, 0);
};
global.cancelAnimationFrame = function (id) {
  clearTimeout(id);
};
copyProps(window, global);