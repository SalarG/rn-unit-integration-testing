/* @jest-environment jsdom */
import React from 'react';
import Enzyme, { shallow, mount, render } from 'enzyme';
import { Text, TextInput } from 'react-native';
import AuthScreen from '../src/AuthScreen';
import Root from '../src/Root';

//jest.mock('react-navigation', () => ({ withNavigation: component => component }));
jest.mock('react-navigation', () => {
    return {
        navigation: { navigate: jest.fn() },
        createAppContainer: jest.fn().mockReturnValue(function NavigationContainer(props) {return null;}),
        createDrawerNavigator: jest.fn(),
        createMaterialTopTabNavigator: jest.fn(),
        createStackNavigator: jest.fn(),
        StackActions: {
            push: jest.fn().mockImplementation(x => ({...x,  "type": "Navigation/PUSH"})),
            replace: jest.fn().mockImplementation(x => ({...x,  "type": "Navigation/REPLACE"})),
        },
        NavigationActions: {
            navigate: jest.fn().mockImplementation(x => x),
        },
        
    }
});

const mockProps = {
    navigation: { navigate: jest.fn() },
    loginSuccess: jest.fn(),
    isSubmitting: true
}

describe('auth screen', () => {
    let wrapped;

    beforeEach(() => {
        wrapped = mount(
            <Root>
                <AuthScreen {...mockProps} />
            </Root>
        );
    })

    afterEach(() => {
        wrapped.unmount()
    })

    it('should contain text', () => {

        expect(wrapped.findWhere(node => node.prop('testID') === 'emailTitle').first().text()).toBe('Email');
    });

    it('should simulate textInput', () => {

        const text = 'test@app.com';
        wrapped.findWhere(node => node.prop('testID') === 'enterEmail').first().props().onChangeText(text);
        wrapped.update();
        expect(wrapped.findWhere(node => node.prop('testID') === 'enterEmail').first().props().value).toBe('test@app.com');
    });

    it('when tap login, textInput gets emptied', () => {
        const text = 'test@test.com';
        wrapped.findWhere(node => node.prop('testID') === 'enterEmail').first().props().onChangeText(text);
        wrapped.update();
        expect(wrapped.findWhere(node => node.prop('testID') === 'enterEmail').first().props().value).toBe('test@test.com');
        wrapped.findWhere(node => node.prop('testID') === 'authButton').first().props().onPress();
        wrapped.update();
        expect(wrapped.findWhere(node => node.prop('testID') === 'enterEmail').first().props().value).toBe('');
    })


})

// it('tests the modified initial state', () => {
//     const initialState = false;
//     const wrapped = mount(
//         <Root initialState={initialState}>
//             <AuthScreen {...mockProps} />
//         </Root>
//     )
//     expect(wrapped.findWhere(node => node.prop('testID') === 'authButton').first().props().title).toBe('Login')

// })